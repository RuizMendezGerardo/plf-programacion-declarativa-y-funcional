Mapa 1: Programación declarativa
```plantuml
@startmindmap
* Lenguaje de programación funcional

** Es un paradigma que surge como reaccion a algunos problemas que lleva la programacion imperativa
*** Libera las asignaciones, detalla el control de la gestion de memoria y utilizar otros recursos expresivos que permitan especificar los programas a un nivel mas alto y mas cercano a la forma de pensar del programador
*** Las tareas rutinarias de programacion se dejan al compilador
*** Ayuda a disminuir la parte burocratica de la programacion, abandonando las ordenes que gestionan la memoria del ordenador
*** Conseguir reducir la complejidad de los programas, por lo tanto el riesgo y lo confuso del codigo escrito
*** Se puede entender como tercera evolucion de los lenguajes de programacion, sucediendo a los lenguajes de alto nivel
*** Ayuda al entendimiento del paradigma imperativo, todo lo complejo lo vuelve natural
*** Al eliminar lo complejo es necesario buscar alternativas expresivas

** Hay dos variables del mismo

*** Funcional
**** Recurre al lenguaje que utilizal los matemáticos al definir funciones
**** Nos escucha y solo ejecuta solo aquello que necesita procesar para dar una respuesta
**** Ofrece como solución las funciones de orden superior (definir funciones que funcionas sobre otras funciones)

*** Lógica
**** Se acude a la logica de predicados de primer orden
**** Es un modelo que se basa en la demostracion de la logica con axiomas y reglas de inferencia
**** Se basan en la relaciones entre objetos, estas relaciones no establecen orden entre argumentos de entrada y datos de salida
**** permite ser mas declarativo puesto que no establecemos una direccion de procesamiento
**** En resumen, nos responde si es verdadero o falso en base a un conjunto de relaciones, condiciones e información que le demos
***** Por ejemplo, si un número es divisor de otro

** Tiene ciertas ventajas sobre la programación imperativa
*** Los programas escritos son mucho mas cortos en comparación
*** El tiempo de desarrollo es una decima parte que en uno imperativo
*** Los programas son mas faciles de realizar y depurar
*** No es necesario recordar detalles al igual que en el imperativo, ya que no están ahí
*** Todo lo complejo se vuelve natural

** A su vez, tiene ciertas desventajas
*** Son ideas en principio teóricas, siendo difícil llevar a la práctica
*** Es posible que el programa no sea tan eficiente como podría
*** si uno describe sus programas a un nivel demasiado alto es el compilador el que toma todas las deciciones de forma automática
*** Hay que tener habilidad para saber a que nivel trabajar
@endmindmap
```

Mapa 2: Lenguaje de programación funcional
```plantuml
@startmindmap
* Lenguaje de programación funcional

** Evolución histórica
*** En 1930 se desarrollo el Lambda calculo por Stephen Kleene, pensado como sistema para estudiar el concepto de funciones obteniendo un sistema computacional bastante potente equivalente al de Neumann
*** En base a estos trabajos, Haskell Brooks Curry desarrollo la lógica combinatoria
*** En 1965, Peter landin modeló las bases de la programacion funcional gracias a los trabajos ya mencionados

** Un paradigma de programación es el modelo de computación en el que los diferentes lenguajes dotan de semantica a los programas.\nUn ejemplo de paradigma es el paradigma imperativo, basado en lo que proponia Von Neumann 

*** "Los programas deben ser almacenados en la maquina antes de ser ejecutados"
*** "Su ejecución consistiría en una serie de instrucciones ejecutadas secuencialmente"
*** "Esas secuencias se podrán modificar accediendo a un espacio de memoria llamado variables"
*** "Las variables podrán ser modificadas mediante instrucciones de asignación"
*** La mayoría de lenguajes siguen este modelo
*** Aunque la programacion orientada a objetos se basa en pequeños trozos de codigo interactuando entre sí, es una variante a este paradigma

** Funciones 
*** Mientras en el paradigma imperativo un programa es una serie de instrucciones que opera sobre datos, el paradigma funcional se basa unicamente en funciones puramente matematicas
*** Estas funciones reciben parametros de entrada y devuelven una salida.
*** Al trabajar puramente con funciones, cambia por completo el concepto de asignación: cosas como "x=x+2" son inexistentes
*** Al igual que desaparecen todos los conceptos basados en las mismas, como son los bucles
*** Al usar este paradigma debemos buscar otra forma de resolver los problemas, siendo la recursión
**** Estas son funciones que se hacen referencias a si mismas, por ejemplo el factorial
*** Todo en este paradigma gira en torno a las funciones, llamandose "de primera clase"
*** Pueden incluso ser parámetros y salidas de otras funciones
*** Moses Schönfinkel y Gottlob crearon el termino currificación, que consiste en transformar una función que utiliza múltiples\n argumentos en una secuencia de funciones que utilizan un único argumento
*** Las funciones se evaluan de forma perezosa, se guarda el resultado de una operación para no ser calculada mas de una vez
**** Este surge del concepto Memoizacion, creado en 1968 por Donald Michie 
**** Lo podemos entender como "No trabaja de más, pero tiene buena memoria"
**** Esto nos permite hacer cosas que no se podrían con otros paradigmas


** Variables
*** Al haber cambiado el concepto de asignación, todo el concepto de variable es modificado por completo
*** Ahora solo sirven para referirse a los parametros de entrada de las funciones
*** Al ser devuelta la salida de la función, la variable de entrada desaparece por completo
*** Al cambiar tanto, es de lo más complicado a entender para alguien que inicia en este paradigma

** Constantes
*** Ahora las constantes se equiparan a funciones
*** Si una funcion devuelve siempre el mismo resultado se puede llamar funcion constante
*** Su resultado es independiente de los parametros de entrada
*** Todas las funciones que no reciben parámetros son funciones constantes

** Ventajas
*** Al no tener variables que pueden cambiar en cualquier momento, los programas solo dependen de los parametros de entrada
**** Esto se conoce como "Transparencia referencial". Las funciones son independiente del orden en que se realicen los cálculos
*** Funciona mejor en un entorno multiprocesador, al depender solo de las entradas puedes ejecutarlas en paralelo sin problemas
*** Nos permiten realizar prototipos muy rápidamente, con este paradigma es más rápida que con otros, aunque con un coste más alto
**** Aqui debemos analizar si necesitamos analizar el programa muchas veces o tener un programa rapido y decidir que paradigma deseamos 
*** Se puede aplicar a cualquier campo, por ejemplo resolver un sudoku en menos de 20 lineas
@endmindmap
```